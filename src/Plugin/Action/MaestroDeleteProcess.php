<?php

namespace Drupal\vbo_maestro\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\maestro\Engine\MaestroEngine;
use Drupal\maestro\Entity\MaestroProcess;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Delete a maestro process.
 *
 * @Action(
 *   id = "vbo_maestro_delete_process",
 *   label = @Translation("Delete selected processes and associated data."),
 *   type = "maestro_process",
 *   confirm = TRUE,
 * )
 */
class MaestroDeleteProcess extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity instanceof MaestroProcess) {
      MaestroEngine::deleteProcess($entity->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $account->hasPermission('delete maestro process entities')
      ? AccessResult::allowed() : $object->access('delete', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
