# VBO Actions for Maestro

The VBO Actions for Maestro module provides various Maestro actions for View
Bulk Operations (VBO) in Drupal. It extends the functionality of Views Bulk
Operations by integrating with Maestro to perform custom actions on multiple
entities at once.

For a full description of the module, visit the [project page](https://www.drupal.org/project/vbo_maestro).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/vbo_maestro).


## Table of Contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

This module requires the following dependencies:

- [Maestro](https://www.drupal.org/project/maestro)
- [Views Bulk Operations](https://www.drupal.org/project/views_bulk_operations)
- Drupal core version: ^9.5 || ^10 || ^11


## Installation

1. Download the module files or install via Composer.
2. Place the module files in the `modules` directory of your Drupal installation.
3. Enable the module through the Drupal administration interface at Administration > Extend.


## Configuration

1. Ensure that Maestro and Views Bulk Operations modules are enabled.
2. Configure your views with VBO and add Maestro actions to perform bulk operations on entities.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
